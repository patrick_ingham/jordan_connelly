﻿using UnityEngine;
using System.Collections;

public class Destroyer : BoardSquare {
	
	protected override void Awake () {
		base.Awake ();
		totalHits = 2;
		images [2] = Resources.Load ("Destroyer") as Texture2D;
		bonusAmmo = 0;
	}

	protected override void onSquareSelected ()
	{
		Destroyer[] destroyers = GameObject.FindObjectsOfType<Destroyer> () as Destroyer[];
		foreach (Destroyer destroyer in destroyers) {
			if (destroyer.hit && destroyer.Number == this.Number) {
				totalHits--;
			}
		}		
		if (totalHits == 0) {
			updateEndGraphic (this);
			updateDestroyShip  (this);
		}
	}

	protected override void updateDestroyShip(BoardSquare ship)
	{
		GameController.instance.shipsLeftCounter();
		GameController.instance.shotsLeftCounter(bonusAmmo);
	}

	protected override void updateEndGraphic (BoardSquare ship)
	{
		Destroyer[] destroyers = GameObject.FindObjectsOfType<Destroyer> () as Destroyer[];
		foreach (Destroyer destroyer in destroyers) {
			if (destroyer.Number == ship.Number) {
				destroyer.GetComponent<Renderer> ().material.color = new Color (1, .5f, .5f);
				destroyer.GetComponent<Renderer> ().material.mainTexture = images [2];	
			}
		}
	}

}
