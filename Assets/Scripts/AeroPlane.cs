using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AeroPlane : BoardSquare {

	public static float RadarDistance { get { return 1.5f; } }
	
	protected override void Awake () {
		base.Awake();
		totalHits = 1;
		images[1] = images[2] = Resources.Load ("Plane") as Texture2D;
		bonusAmmo = 0;
	}
	
	protected override void onSquareSelected() {
		totalHits--;
		
		if(totalHits==0) {
			updateEndGraphic(this);
			updateDestroyShip  (this);
		}
	}

	protected override void updateDestroyShip(BoardSquare ship)
	{
		BoardSquare[] nearestSquares = getNearestSquares(this);
		foreach(BoardSquare square in nearestSquares)
		{
			square.foundOnRadar();
		}

	}

	protected override void updateEndGraphic(BoardSquare ship){
		ship.GetComponent<Renderer>().material.color = new Color(1, .5f, .5f);
		ship.GetComponent<Renderer>().material.mainTexture = images[2];
	}

	private BoardSquare[] getNearestSquares(BoardSquare plane)
	{
		BoardSquare[] squares = FindObjectsOfType<BoardSquare>() as BoardSquare[];
		List<BoardSquare> nearest = new List<BoardSquare>();

		foreach (BoardSquare square in squares)
		{
			if (Vector3.Distance(square.transform.position, plane.transform.position) < AeroPlane.RadarDistance && square != plane)
			{
				nearest.Add(square);
			}
		}

		return nearest.ToArray();
	}

}
