﻿using UnityEngine;
using System.Collections;

public class Sea : BoardSquare {

	protected override void Awake () {
		base.Awake();
		totalHits = 0;
		images[1] = images[2] = Resources.Load ("Fish") as Texture2D;
		bonusAmmo = 0;
	}
	
	protected override void onSquareSelected() {
		if (totalHits == 0) {
			updateEndGraphic (this);
		}
	}

	protected override void updateDestroyShip(BoardSquare ship)
	{
		//does nothing as its the SEA square
	}

	protected override void updateEndGraphic(BoardSquare ship){
		ship.GetComponent<Renderer>().material.mainTexture = images[2];
	}
}