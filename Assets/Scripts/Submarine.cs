using UnityEngine;
using System.Collections;

public class Submarine : BoardSquare {
	
	protected override void Awake () {
		base.Awake();
		totalHits = 1;
		images[2] = Resources.Load ("Submarine") as Texture2D;
		bonusAmmo = 0;
	}
	
	protected override void onSquareSelected() {
		Submarine[] submarines = GameObject.FindObjectsOfType<Submarine>() as Submarine[];
		foreach(Submarine submarine in submarines) {
			if(submarine.hit && submarine.Number == this.Number) {
				totalHits--;
			}
		}
		if(totalHits==0) {
			updateEndGraphic(this);
			updateDestroyShip  (this);
		}
	}

	protected override void updateDestroyShip(BoardSquare ship)
	{
		GameController.instance.shipsLeftCounter();
		GameController.instance.shotsLeftCounter(bonusAmmo);
	}

	protected override void updateEndGraphic(BoardSquare ship){
		ship.GetComponent<Renderer>().material.color = new Color(1, .5f, .5f);
		ship.GetComponent<Renderer>().material.mainTexture = images[2];
	}
}
